
#MCU_TYPE=M7
#MCU_TYPE=M4

ifeq ($(MCU_TYPE),M7)
	TARGET = main_m7
	MCU_SPEC = cortex-m7
	FPU_SPEC = fpv5-d16
else
	TARGET = main_m4
	MCU_SPEC = cortex-m4
	FPU_SPEC = fpv5-sp-d16
endif

TOOLCHAIN = ./gcc-arm-none-eabi-10-2020-q4-major/bin
CC = $(TOOLCHAIN)/arm-none-eabi-gcc
CPP = $(TOOLCHAIN)/arm-none-eabi-g++
AS = $(TOOLCHAIN)/arm-none-eabi-as
LD = $(TOOLCHAIN)/arm-none-eabi-ld
OC = $(TOOLCHAIN)/arm-none-eabi-objcopy
OD = $(TOOLCHAIN)/arm-none-eabi-objdump
OS = $(TOOLCHAIN)/arm-none-eabi-size

# Assembly directives.
ASFLAGS += -mcpu=$(MCU_SPEC)
ASFLAGS += -mthumb

# C compilation directives
CFLAGS += -mcpu=$(MCU_SPEC)
CFLAGS += -mthumb
CFLAGS += -mhard-float
CFLAGS += -mfloat-abi=hard
CFLAGS += -mfpu=$(FPU_SPEC)
CFLAGS += -Wall
CFLAGS += -g
CFLAGS += -Os
CFLAGS += -fmessage-length=0 -fno-common
CFLAGS += -ffunction-sections -fdata-sections
CFLAGS += -std=c99

# C++ compilation directives
CPPFLAGS += -mcpu=$(MCU_SPEC)
CPPFLAGS += -mthumb
CPPFLAGS += -mhard-float
CPPFLAGS += -mfloat-abi=hard
CPPFLAGS += -mfpu=$(FPU_SPEC)
CPPFLAGS += -Wall
CPFLAGS += -g
CPPFLAGS += -Os
CPPFLAGS += -fmessage-length=0 -fno-common
CPPFLAGS += -ffunction-sections -fdata-sections
CPPFLAGS += -fno-exceptions

# Linker directives.
LSCRIPT = ./$(LD_SCRIPT)
LFLAGS += -mcpu=$(MCU_SPEC)
LFLAGS += -mthumb
LFLAGS += -mhard-float
LFLAGS += -mfloat-abi=hard
LFLAGS += -mfpu=$(FPU_SPEC)
LFLAGS += -Wall
LFLAGS += --static
LFLAGS += -Wl,-Map=$(TARGET).map
LFLAGS += -Wl,--gc-sections
LFLAGS += -lgcc
LFLAGS += -lc
LFLAGS += -T$(LSCRIPT)

ifeq ($(MCU_TYPE),M7)
# Source files M7
C_SRC += ./CM7/Src/cm4_code.c
C_SRC += ./CM7/Src/main.c
C_SRC += ./CM7/Src/stm32h7xx_hal_msp.c
C_SRC += ./CM7/Src/stm32h7xx_it.c

INCLUDE += -I./CM7/Inc

DEFS += -DCORE_CM7

LD_SCRIPT = ./CM7/STM32H745XIHx_FLASH_CM7.ld
else
# Source files M4
C_SRC += ./CM4/Src/main.c
C_SRC += ./CM4/Src/stm32h7xx_hal_msp.c
C_SRC += ./CM4/Src/stm32h7xx_it.c

INCLUDE += -I./CM4/Inc

DEFS += -DCORE_CM4

LD_SCRIPT = ./CM4/STM32H745XIHx_FLASH_CM4.ld
endif

# Source files common
AS_SRC = ./Common/startup_stm32h745xx.s

C_SRC += ./Common/Src/system_stm32h7xx.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_rcc_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_cortex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_pwr_ex.c
C_SRC += ./Drivers/STM32H7xx_HAL_Driver/Src/stm32h7xx_hal_gpio.c
C_SRC += ./Drivers/BSP/STM32H745I-DISCO/stm32h745i_discovery.c

INCLUDE += -I./Common/Inc
INCLUDE += -I./Drivers
INCLUDE += -I./Drivers/CMSIS/Include
INCLUDE += -I./Drivers/CMSIS/Device/ST/STM32H7xx/Include
INCLUDE += -I./Drivers/STM32H7xx_HAL_Driver/Inc
INCLUDE += -I./Drivers/BSP/STM32H745I-DISCO

DEFS += -DSTM32H745xx -DUSE_HAL_DRIVER

OBJS = $(C_SRC:.c=.o)
OBJS += $(CPP_SRC:.cpp=.o)
OBJS += $(AS_SRC:.s=.o)

.PHONY: all
all: $(TARGET).bin

%.o: %.s
	$(AS) $(ASFLAGS) -c $< -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDE) $(DEFS) $< -o $@

%.o: %.cpp
	$(CPP) -c $(CPPFLAGS) $(INCLUDE) $(DEFS) $< -o $@

$(TARGET).elf: $(OBJS)
	$(CPP) $^ $(LFLAGS) -o $@

$(TARGET).bin: $(TARGET).elf
	$(OC) -S -O binary $< $@
	$(OS) $<

.PHONY: clean
clean:
	rm -f $(OBJS)
	rm -f $(TARGET).elf
	rm -f $(TARGET).bin
	rm -f $(TARGET).map