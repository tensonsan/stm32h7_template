## VSCode gcc makefile template project for the STM32H745I-DISCO board

### Usage:

You need the [VSCode](https://code.visualstudio.com/).

Compile the code into an ELF executable.
```Bash
./build.sh
```

Note: Cross-compliation is done in two steps. The M4 core binary is build first. 
The M4 binary is then integrated into the M7 binary during the second build. 
The resulting binary is flashed to the MCU using the [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) software.

Note: You have to connect both CN15 and CN14 to the PC to power the board via USB as the STM32H745I-DISCO consumes a lot of power.

After flashing each core is toggling it's own LED every 0.5 seconds.
